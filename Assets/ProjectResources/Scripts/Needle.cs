﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needle : MonoBehaviour
{
    public Transform PushStick;
    public Vector3 PushStickStartPosition;
    public Vector3 PushStickEndPosition;

    private float _pushStickSpeed = 0.1f;
    private float _distanceTraveled = -0.8f;

    // Total distance between the markers.
    private float _journeyLength;

    public bool isPushingIn = true;

    void Start()
    {

        Push();
        _journeyLength = Vector3.Distance(PushStickStartPosition, PushStickEndPosition);
    }   

    // Update is called once per frame
    void Update()
    {
        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = (Time.time * _pushStickSpeed) / _journeyLength;

        PushStick.localPosition = Vector3.Lerp(PushStickStartPosition, PushStickEndPosition, fracJourney);

        if (!isPushingIn)
        {
            PushOut();
            isPushingIn = !isPushingIn;
        }

    }

    void Push()
    {
        PushStickStartPosition = PushStick.localPosition;
        PushStickEndPosition = PushStick.localPosition + new Vector3(0f, _distanceTraveled, 0f);
    }

    void PushOut()
    {
        _distanceTraveled *= -1f;
        Push();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.GetComponent<NeedleInteractable>())
        {

            if (!collision.transform.GetComponent<NeedleInteractable>().IsNeedleIsRightPlace)
            {
                Debug.Log(" Wrong Place ");
            }
            else
            {
                Debug.Log(" Good Jooooooooobbbbbbbbb!!!!!");
            }
        }
    }
}
