# FunShot

A vr experience that allows kids (patients) to give virtual shots.
This way they can get used to giving shots.
By gaining experience they will lose their fear of getting a shot making the process much faster and easier for physicians